import { NoAutocomplete } from "./mixins/no-autocomplete.mjs";
const { ApplicationV2, HandlebarsApplicationMixin } = foundry.applications.api;

/**
 * Apply Damage Prompt
 *
 * Handles damage application UX when adjustment is desired.
 *
 * Calls {@link ActorPF#applyDamage} when finally committing changes.
 *
 * Called primarily from {@link ActorPF.applyDamage}
 */
export class ApplyDamage extends HandlebarsApplicationMixin(NoAutocomplete(ApplicationV2)) {
  /** @type {number} */
  value;
  /** @type {boolean} */
  isHealing;
  /** @type {boolean} */
  asWounds;
  /** @type {number} */
  critMult;
  /** @type {boolean} */
  asNonlethal;
  /** @type {number} */
  reduction;
  /** @type {Collection<object>} */
  targets;
  /** @type {Array} */
  instances;
  /** @type {Function} */
  resolve;

  constructor(options) {
    const isHealing = options.value < 0;
    if (isHealing) options.value = -options.value;

    const { resolve } = options;

    super(options);

    this.damageOptions = {
      asNonlethal: options.asNonlethal ?? false,
      asWounds: options.asWounds ?? false,
      critMult: options.critMult ?? 0,
      dualHeal: options.dualHeal ?? game.settings.get("pf1", "dualHeal"),
    };

    Object.defineProperties(this, {
      resolve: {
        value: resolve,
        writable: true,
        enumerable: false,
      },
      isHealing: {
        value: isHealing,
        writable: false,
      },
      asWounds: {
        value: options.asWounds ?? false,
      },
      asNonlethal: {
        value: options.asNonlethal ?? false,
      },
      critMult: {
        value: options.critMult || 0,
      },
      value: {
        value: options.value,
        writable: true,
      },
      reduction: {
        value: options.reduction,
        writable: true,
      },
      woundsAndVigor: {
        value: false,
        writable: true,
      },
    });

    this._prepareInstances();
    this._prepareTargets();
  }

  static DEFAULT_OPTIONS = {
    tag: "form",
    classes: ["pf1-v2", "apply-damage", "standard-form"],
    window: {
      resizable: true,
      minimizable: false,
    },
    position: {
      width: 460,
      height: "auto",
    },
    actions: {
      apply: this._onApply,
    },
  };

  get title() {
    return this.isHealing ? game.i18n.localize("PF1.ApplyHealing") : game.i18n.localize("PF1.ApplyDamage");
  }

  static PARTS = {
    form: {
      template: "systems/pf1/templates/apps/apply-damage-dialog.hbs",
    },
    footer: {
      template: "templates/generic/form-footer.hbs",
    },
  };

  _preparePartContext(partId, context, options) {
    switch (partId) {
      case "footer": {
        context.buttons = [
          {
            action: "apply",
            type: "submit",
            label: game.i18n.localize("PF1.Apply"),
            icon: "fa-solid fa-heart-pulse",
            default: true,
          },
        ];
        break;
      }
      case "form": {
        context.value = this.value;
        context.isHealing = this.isHealing;
        context.reduction = this.reduction;
        context.targets = this.targets;
        context.instances = this.instances;
        context.hasPhysical = this.hasPhysical;
        context.hasEnergy = this.hasEnergy;
        context.options = this.damageOptions;
        context.useWoundsAndVigor = this.woundsAndVigor;
        break;
      }
    }

    return context;
  }

  /**
   * @override
   * @param {object} options
   * @param {Event} event
   */
  _onChangeForm(options, event) {
    const formData = foundry.utils.expandObject(new FormDataExtended(this.element).object);

    const name = event.target.name;
    const parts = name.split(".");
    if (parts[0] === "targets") {
      const idx = parts[1];
      const data = formData.targets[idx];
      this._updateTarget(data.uuid, data);
    }

    switch (name) {
      case "value":
        this.value = formData.value;
        break;
      case "reduction":
        this.reduction = formData.reduction;
        break;
    }

    for (const target of this.targets) this._updateTarget(target.uuid);

    // Merge options
    foundry.utils.mergeObject(this.damageOptions, formData.options);

    // Re-render if no action was triggered
    if (!event.target.dataset?.action) this.render({ parts: ["form"] });
  }

  /** @override */
  _onClose() {
    super._onClose();

    this.resolve?.(null);
  }

  /**
   * Prepare damage/healing instances.
   *
   * Merge damage instances of same type.
   */
  _prepareInstances() {
    const instances = this.options.instances;

    for (let i = 1; i < instances.length; i++) {
      const cur = instances[i];
      if (cur.value == 0) continue;

      // Merge with previous if one exists
      for (let y = 0; y < i; y++) {
        const prev = instances[y];
        if (prev.value == 0) continue;
        if (cur.types.equals(prev.types)) {
          prev.value += cur.value;
          cur.value = 0;
          break;
        }
      }
    }

    Object.defineProperty(this, "instances", {
      value: instances.filter((i) => i.value != 0),
      writable: false,
      enumerable: false,
    });
  }

  _prepareTargets() {
    const instances = this.instances ?? [];
    for (const instance of instances) {
      instance.label = pf1.utils.i18n.join(instance.names, "u", true);
    }

    const unknownTypes =
      instances.length === 0 ||
      instances.some((i) => i.standard.some((t) => !["physical", "energy"].includes(t.category)));

    Object.defineProperties(this, {
      hasPhysical: {
        value: unknownTypes ? true : instances.some((i) => i.standard.some((t) => t.category === "physical")),
        enumerable: false,
        writable: false,
      },
      hasEnergy: {
        value: unknownTypes ? true : instances.some((i) => i.standard.some((t) => t.category === "energy")),
        enumerable: false,
        writable: false,
      },
    });

    Object.defineProperty(this, "targets", {
      value: new Collection(),
      writable: false,
      enumerable: false,
    });

    /** @type {pf1.applications.settings.HealthConfigModel} */
    const hpconfig = game.settings.get("pf1", "healthConfig");

    for (const actor of this.options.targets) {
      const target = {
        uuid: actor.uuid,
        actor,
        name: actor.token?.name || actor.name,
        value: this.value,
        isToken: !!actor.token,
        ratio: 1,
        dr: this.hasPhysical ? this.getResistances(actor, "dr") : [],
        get activeDR() {
          return this.dr.some((r) => r.enabled);
        },
        eres: this.hasEnergy ? this.getResistances(actor, "eres") : [],
        get activeER() {
          return this.eres.some((r) => r.enabled);
        },
        hardness: actor.system.traits.hardness,
        reduction: undefined,
        reductionChoices: 0, // Display choices
        di: actor.system.traits?.di?.names ?? [],
        dv: actor.system.traits?.dv?.names ?? [],
        selected: true,
      };

      target.haveDIV = target.di.length || target.dv.length;
      target.haveDER = target.dr.length || target.eres.length || target.hardness;

      this.targets.set(target.uuid, target);

      this._updateTarget(target.uuid);

      const cfg = hpconfig.getActorConfig(actor);
      this.woundsAndVigor ||= cfg.rules.useWoundsAndVigor ?? false;
    }
  }

  /**
   *
   * @param {ActorPF} actor
   * @param {"dr"|"eres"} resistanceType
   * @returns {object[]} - Array of results from {@link ActorPF._parseResistance()}
   */
  getResistances(actor, resistanceType = "dr") {
    const res = actor.system.traits?.[resistanceType];
    if (!res) return;

    const resistances = [];

    if (res.custom) {
      for (const entry of res.custom.split(pf1.config.re.traitSeparator)) {
        const re = actor._parseResistanceEntry(entry, resistanceType);
        resistances.push(re);
      }
    }
    if (res.value?.length) {
      for (const entry of res.value) {
        const re = actor._parseResistanceEntry(entry, resistanceType);
        resistances.push(re);
      }
    }

    const damageTypes = new Set(this.instances.map((i) => [...i.types]).flat());

    // Add in data used by this dialog
    const enrichResistance = (res) => {
      res.value = res.amount;
      res.active = false;
      res.typeIds = [res.type0?.id, res.type1?.id].filter((t) => t);
      res.enabled = true;
      res.hasGeneric = res.isDR && (res.typeIds.length === 0 || res.typeIds.includes(""));

      // Set generic resistances to be active by default
      if (res.hasGeneric) {
        res.active = true;
      } else {
        // Operator: true -> OR; false -> AND
        if (res.operator) {
          // OR; if any of the resistance types are in the damage types, the resistance is overcome
          res.active = !res.typeIds.some((t) => damageTypes.has(t));
        } else {
          // AND; if all of the resistance types are in the damage types, the resistance is overcome
          res.active = !res.typeIds.every((t) => damageTypes.has(t));
        }
      }
    };

    for (const res of resistances) enrichResistance(res);

    return resistances;
  }

  /**
   * Update target data based on form data and recalculate values.
   *
   * @internal
   * @param {string} targetId
   * @param {object} [data] - Update data
   */
  _updateTarget(targetId, data) {
    const target = this.targets.get(targetId);
    if (data) {
      const { dr, eres } = data;
      delete data.dr;
      delete data.eres;

      foundry.utils.mergeObject(target, data);

      if (dr) Object.values(dr).forEach((rdata, idx) => foundry.utils.mergeObject(target.dr[idx], rdata));
      if (eres) Object.values(eres).forEach((rdata, idx) => foundry.utils.mergeObject(target.eres[idx], rdata));
    }

    // Update value
    target.value = Math.floor(this.value * (target.ratio ?? 0));
    // Reduction input
    const baseReduction = (target.reduction || 0) + (this.reduction || 0);
    // Reduction choices
    let reductionChoices = 0;
    reductionChoices += target.dr.filter((r) => r.active).reduce((total, cur) => total + cur.value, 0);
    reductionChoices += target.eres.filter((r) => r.active).reduce((total, cur) => total + cur.value, 0);

    target.reductionChoices = reductionChoices;

    // Reduce and clamp
    target.value -= baseReduction + reductionChoices;
    if (target.value < 0) target.value = 0;
  }

  /**
   * Apply health change
   *
   * @param {Event} event
   * @param {HTMLElement} target
   * @this {ApplyDamage}
   */
  static async _onApply(event, target) {
    event.preventDefault();

    const resolve = this.resolve;
    this.resolve = null; // Prevent _onClose() calling resolve
    const config = {
      value: this.options.value,
      reduction: this.reduction || 0,
      isHealing: this.isHealing,
      asWounds: this.asWounds,
      asNonlethal: this.asNonlethal,
      instances: this.instances,
      targets: this.targets,
    };

    // This causes clicks to go through the dialog
    this.element.inert = true;

    // Applied value with general damage reduction accounted for
    let value = Math.max(0, this.value - (this.reduction || 0));
    if (this.isHealing) value = -value; // Flip healing

    const updated = [];
    try {
      const promises = [];

      let targetCount = 0;
      for (const target of this.targets) {
        if (target.value == 0) continue;
        targetCount++;
        const p = target.actor.applyDamage(value, this._getTargetDamageOptions(target));
        updated.push(target.actor);
        promises.push(p);
      }

      await Promise.all(promises);

      value = Math.abs(value);
      if (this.isHealing)
        ui.notifications.info(game.i18n.format("PF1.Application.Damage.ApplyHeal", { targets: targetCount }));
      else ui.notifications.info(game.i18n.format("PF1.Application.Damage.ApplyDamage", { targets: targetCount }));
    } catch (err) {
      ui.notifications.error(err.message, { console: false });
      throw err;
    } finally {
      this.close();

      config.updated = updated;
      resolve(config);
    }
  }

  _getTargetDamageOptions(target) {
    const options = pf1.utils.deepClone({
      ...this.damageOptions, // Spread damage options in, allowing modules to extend the options and slip in extra data
      reduction: (target.reduction || 0) + (this.reduction || 0) + target.reductionChoices,
      ratio: target.ratio ?? 1,
      event: this.options.event,
      message: this.options.message,
      element: this.options.element,
      instances: this.instances,
    });
    return options;
  }

  /**
   *
   * @param {object} options - Options passed to apply damage
   * @param {object} [renderOptions] - Render options passed to the application.
   * @returns {Promise<null|object>} - Null if cancelled, object describing applied settings.
   */
  static async wait(options, renderOptions = {}) {
    return new Promise((resolve) => {
      options.resolve = resolve;
      const app = new this(options);
      app.render(true, renderOptions);
    });
  }
}
