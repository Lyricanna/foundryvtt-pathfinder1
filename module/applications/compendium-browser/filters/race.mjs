import { CheckboxFilter } from "./checkbox.mjs";

export class CreatureTypeFilter extends CheckboxFilter {
  static label = "PF1.CreatureType";
  static type = "race";
  static indexField = "system.creatureTypes";

  /** @inheritDoc */
  prepareChoices() {
    this.choices = this.constructor.getChoicesFromConfig(pf1.config.creatureTypes);
  }
}

export class CreatureSubTypeFilter extends CheckboxFilter {
  static label = "PF1.RaceSubtypePlural";
  static type = "race";
  static indexField = "system.creatureSubtypes";

  /** @inheritDoc */
  prepareChoices() {
    super.prepareChoices();

    // Merge standard config choices with custom choices
    const choices = this.choices;
    const configChoices = this.constructor.getChoicesFromConfig(pf1.config.creatureSubtypes);
    for (const choice of configChoices) {
      choices.set(choice.key, choice);
    }
    this.choices = choices;
  }
}
