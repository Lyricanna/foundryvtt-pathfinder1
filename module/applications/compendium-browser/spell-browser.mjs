import { CompendiumBrowser } from "./compendium-browser.mjs";
import * as commonFilters from "./filters/common.mjs";
import * as spellFilter from "./filters/spell.mjs";

export class SpellBrowser extends CompendiumBrowser {
  static typeName = "PF1.Spells";
  static types = ["spell"];
  static filterClasses = [
    commonFilters.PackFilter,
    spellFilter.SpellSchoolFilter,
    spellFilter.SpellSubschoolFilter,
    spellFilter.SpellDescriptorFilter,
    spellFilter.SpellLearnedByClassFilter,
    spellFilter.SpellLearnedByDomainFilter,
    spellFilter.SpellLearnedBySubdomainFilter,
    spellFilter.SpellLearnedByBloodlineFilter,
    spellFilter.SpellLevelFilter,
    commonFilters.TagFilter,
  ];
  /** @override */
  static _mapEntry(entry, pack) {
    const result = super._mapEntry(entry, pack);

    /** @type {number[]} */
    const learnedAtLevels = Object.values(entry.system.learnedAt ?? {})
      .map((learnedAtSource) => Object.values(learnedAtSource))
      .flat();
    if (typeof entry.system.level === "number") learnedAtLevels.push(entry.system.level);
    // NOTE: This results in `level` being a number[] instead of a number like in the source data.
    result.system.level = [...new Set(learnedAtLevels)];

    return result;
  }
}
