import { NoAutocomplete } from "./mixins/no-autocomplete.mjs";
import { DragDropApplicationMixin } from "@app/mixins/drag-drop.mjs";

const { ApplicationV2, HandlebarsApplicationMixin } = foundry.applications.api;

export class ExperienceDistributor extends DragDropApplicationMixin(
  HandlebarsApplicationMixin(NoAutocomplete(ApplicationV2))
) {
  static DEFAULT_OPTIONS = {
    tag: "form",
    classes: ["pf1-v2", "experience-distributor", "standard-form"],
    window: {
      title: "PF1.Application.XPDistributor.Title",
      minimizable: true,
      resizable: true,
    },
    actions: {
      split: this._doSplitReward,
      full: this._doFullReward,
    },
    dragDrop: [{ dragSelector: null, dropSelector: "form" }],
    position: {
      width: 450,
      height: 690,
    },
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/apps/experience-distributor.hbs",
      scrollable: [".selectors"],
    },
    footer: {
      template: "templates/generic/form-footer.hbs",
    },
  };

  /**
   * Bonus XP granted
   *
   * @type {number}
   */
  _bonusXP = 0;

  /**
   * Special actor data array
   *
   * @type {ExperienceDistributorActor[]}
   */
  _actors = [];

  constructor(options) {
    if (Array.isArray(options)) {
      foundry.utils.logCompatibilityWarning(
        "ExperienceDistributor constructor first parameter is no longer directly actor array. Please provide options object with actors property instead.",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );
      options = { actors: options };
    }

    super(options);

    const actors = options.actors ?? [];
    this._actors = actors.map((actor) => this.constructor.getActorData(actor)).filter((o) => !!o);
  }

  /* -------------------------------------------- */

  /**
   * @override
   * @internal
   */
  _preparePartContext(partId, context) {
    switch (partId) {
      case "footer":
        context.buttons = [
          {
            action: "split",
            label: "PF1.Application.XPDistributor.SplitEvenly",
            type: "button", // non-default
            icon: "fa-solid fa-people-arrows",
          },
          {
            action: "full",
            label: "PF1.Application.XPDistributor.GiveToAll",
            type: "submit", // default
            icon: "fa-solid fa-star",
          },
        ];
        break;
      case "form":
        // Add combatants
        context.actors = {
          characters: this.characters,
          npcs: this.npcs,
        };
        // Add labels
        context.xp = {
          total: this.totalExperience.toLocaleString(),
          split: this.splitExperience.toLocaleString(),
        };
        context.bonusXP = this._bonusXP;
        break;
    }

    return context;
  }

  /* -------------------------------------------- */

  /**
   * Get PC actors
   *
   * @type {ExperienceDistributorActor[]}
   */
  get characters() {
    return this._actors.filter((o) => !o.isNPC);
  }

  /* -------------------------------------------- */

  /**
   * Get NPC actors
   *
   * @type {ExperienceDistributorActor[]}
   */
  get npcs() {
    return this._actors.filter((o) => o.isNPC);
  }

  /* -------------------------------------------- */

  /**
   * Handle Drop Event
   *
   * @param {DragEvent} event - The originating DragEvent
   * @protected
   * @override
   */
  async _onDrop(event) {
    event.preventDefault();
    const data = TextEditor.getDragEventData(event);

    if (data.type !== "Actor") return;

    // Add actor
    const actor = await pf1.documents.actor.ActorPF.fromDropData(data);

    // Prevent duplicate characters (not NPCs)
    if (actor.type !== "character" || this._actors.find((o) => o.actor === actor) == null) {
      // Add actor to list
      const actorData = this.constructor.getActorData(actor);
      actorData.selected = true;
      this._actors.push(actorData);

      this.render({ parts: ["form"] });
    }
  }

  /* -------------------------------------------- */

  /**
   * The event handler for changes to form input elements
   *
   * @internal
   * @param {ApplicationFormConfiguration} formConfig   The configuration of the form being changed
   * @param {Event} event                               The triggering event
   * @returns {void}
   */
  _onChangeForm(formConfig, event) {
    event.preventDefault();

    const el = event.target;

    if (el.matches("input[type=checkbox]")) {
      const actorID = el.dataset.id;
      const actor = this._actors.find((o) => o.id === actorID);
      if (!actor) return;
      actor.selected = el.checked;
    }

    if (el.matches("input[name=bonusXP]")) {
      this._bonusXP = parseInt(el.value);
      if (isNaN(this._bonusXP)) this._bonusXP = 0;
    }

    this.render({ parts: ["form"] });
  }

  /* -------------------------------------------- */

  /**
   * Split reward for each
   *
   * @param {Event} event
   * @returns
   */
  static _doSplitReward(event) {
    event.preventDefault();
    return this._giveExperience(true);
  }

  /**
   * Full reward for each
   *
   * @internal
   * @param {Event} event
   * @returns
   */
  static _doFullReward(event) {
    event.preventDefault();
    return this._giveExperience(false);
  }

  /**
   * Distributes experience to all PC actors
   *
   * @internal
   * @this {ExperienceDistributor} - XP distributor instance rather than the class.
   * @param {boolean} splitEvenly - Should XP be split evenly?
   * @returns {Promise<void>}
   */
  async _giveExperience(splitEvenly = false) {
    const total = this.totalExperience;
    const split = this.splitExperience;
    const value = splitEvenly ? split : total;
    const characters = this.characters.filter((o) => o.selected);

    if (characters.length == 0) {
      return void ui.notifications.error("PF1.Application.XPDistributor.Warnings.NoRecipients", { localize: true });
    }

    if (!(value > 0)) {
      return void ui.notifications.error("PF1.Application.XPDistributor.Warnings.NoReward", { localize: true });
    }

    if (splitEvenly)
      console.debug("PF1 | XP Reward |", total, "split evenly as", split, "to", characters.length, "characters");
    else console.debug("PF1 | XP Reward |", total, "to all");

    for (const actorData of characters) {
      const result = { value };
      Hooks.callAll("pf1GainXp", actorData.actor, result);
      actorData.value = Math.floor(result.value);
    }

    const updates = characters
      .filter((o) => o.value > 0 && Number.isSafeInteger(o.value))
      .map((o) => ({
        _id: o.actor.id,
        "system.details.xp.value": o.actor.system.details.xp.value + o.value,
      }));

    this.close();

    await Actor.implementation.updateDocuments(updates);

    ui.notifications.info(game.i18n.format("PF1.Application.XPDistributor.Result", { xp: value }), { console: false });
  }

  /* -------------------------------------------- */

  /**
   * Total experience the encounter is worth, including regular XP reward and bonus XP.
   *
   * @returns {number} - Total value
   */
  get totalExperience() {
    const npcs = this.npcs.filter((o) => o.selected);
    return npcs.reduce((cur, o) => cur + o.xp, this._bonusXP);
  }

  /* -------------------------------------------- */

  /**
   * Split experience, as split across all characters.
   *
   * @returns {number} - Reward value
   */
  get splitExperience() {
    const characters = this.characters.filter((o) => o.selected);
    if (characters.length === 0) return 0;
    const xp = this.totalExperience;
    return Math.floor(xp / characters.length);
  }

  /* -------------------------------------------- */

  /**
   * @protected
   * @param {Actor} actor
   * @returns {ExperienceDistributorActor}
   */
  static getActorData(actor) {
    if (!(actor instanceof Actor)) return null;

    const xp = pf1.utils.CR.getXP(actor.getCR?.() ?? 0);

    return {
      id: foundry.utils.randomID(16),
      isNPC: actor.type !== "character",
      actor,
      selected: this._shouldActorBeSelected(actor),
      xp,
      xpLabel: xp.toLocaleString(),
    };
  }

  /* -------------------------------------------- */

  /**
   * Should the actor be selected by default.
   *
   * @param {Actor} actor
   * @returns {boolean}
   */
  static _shouldActorBeSelected(actor) {
    const isPC = actor.type === "character";
    if (isPC) return true;

    /** @type {pf1.applications.settings.HealthConfigModel} */
    const hpConfig = game.settings.get("pf1", "healthConfig");
    const healthConfig = hpConfig.getActorConfig(actor);
    const useWoundsAndVigor = healthConfig.rules.useWoundsAndVigor ?? false;

    return useWoundsAndVigor ? actor.system.attributes?.wounds?.value <= 0 : actor.system.attributes?.hp?.value < 0;
  }

  /* -------------------------------------------- */

  /**
   * Open XP distributor dialog based on passed combat instance.
   *
   * @param {Combat} combat - Combat instance
   * @returns {ExperienceDistributor} - Application instance
   */
  static fromCombat(combat) {
    const app = new this({ actors: combat.combatants.map((c) => c.actor) });

    if (app.characters.length > 0) {
      app.render({ force: true });
    } else {
      app.close();
    }

    return app;
  }
}

/**
 * @typedef {object} ExperienceDistributorActor
 * @property {string} id - Internal reference ID
 * @property {ActorPF} actor - Actor instance
 * @property {boolean} isNPC - Is this an NPC?
 * @property {boolean} selected - Is the actor selected
 * @property {number} xp
 * @property {string} xpLabel
 */
