/**
 * Save edit state
 *
 * @remarks
 * - Must be combined with {@link restoreEditState()}
 * - Static `EDIT_TRACKING` array must be defined to declare what fields are being tracked for editing.
 *
 * @example
 * In sheet body define the following
 * ```js
 * // Init data
 * _activeEdits = {};
 *
 * // Define fields to track
 * static EDIT_TRACKING = ["system.description.value", "system.description.unidentified"];
 *
 * // Override
 * async _render(force, options) {
 *   pf1.applications.utils.saveEditState(this, options);
 *
 *   return super._render(force, options);
 * }
 * ```
 *
 * @internal
 * @experimental
 * @param {FormApplication} sheet
 * @param {object} options
 */
export function saveEditState(sheet, options = {}) {
  if (!sheet.form || !sheet.rendered) return;

  const paths = sheet.constructor.EDIT_TRACKING;

  /** @type {HTMLElement} */
  const isUpdate = options.renderContext === "updateItem";

  // Reset old saved data if it persists
  sheet._activeEdits = {};

  // Save active editor data
  // Save {{editor}} state
  for (const [path, editor] of Object.entries(sheet.editors)) {
    if (!editor.active) {
      delete sheet._activeEdits[path];
    } else {
      sheet._activeEdits[path] = ProseMirror.dom.serializeString(editor.instance.view.state.doc.content);
    }
  }
  // Save <prose-mirror> state, for whenever it works correctly
  /*
  const formData = new FormDataExtended(sheet.form).object;
  for (const path of paths) {
    if (isUpdate && foundry.utils.hasProperty(options.renderData, path)) continue;
    const text = formData[path];
    if (text !== undefined) {
      sheet._activeEdits[path] = text;
      sheet.element[0].classList.add("stale-data");
    }
  }
  */
}

/**
 * Restore edit state
 *
 * @remarks
 * - Must be combined with {@link saveEditState()}
 * - Should use `context.prefix.path.to.data` to set toggled state so the editor is automatically in edit mode.
 * - Prefix defaults to `_editorState` for the above path.
 *
 * @internal
 * @experimental
 * @param {FormApplication} sheet - Application to work with
 * @param {object} context - Context from which to fetch current data
 * @returns {object} - Editor toggled state mapping
 */
export function restoreEditState(sheet, context) {
  const editorToggledState = {};

  for (const path of sheet.constructor.EDIT_TRACKING) {
    foundry.utils.setProperty(editorToggledState, path, true);
  }

  // Re-instate data from in-progress edits
  for (const [path, text] of Object.entries(sheet._activeEdits)) {
    if (!text) continue;
    if (text !== foundry.utils.getProperty(context, path)) {
      foundry.utils.setProperty(context, path, text);
      foundry.utils.setProperty(editorToggledState, path, false);
      console.debug("PF1 | Restored interrupted edit state to", path);

      // Mark {{editor}} as changed
      const editor = sheet.editors[path];
      if (editor) editor.changed = true;
    }
    delete sheet._activeEdits[path];
  }

  sheet._activeEdits = {};

  return editorToggledState;
}

/**
 * Track stale editors
 *
 * Visually marks editors that need action.
 *
 * Call in `activateListeners()`
 *
 * Must be combined with {@link saveEditState()} and {@link restoreEditState()}
 *
 * @internal
 * @experimental
 * @param {FormApplication} sheet
 * @param {HTMLElement} html
 */
export function trackStaleEditors(sheet, html) {
  /** @type {HTMLElement[]} */
  const editors = html.querySelectorAll("prose-mirror.active");
  for (const editor of editors) {
    if (!sheet.constructor.EDIT_TRACKING?.includes(editor.name)) continue;

    editor.classList.add("stale-editor");
    editor.addEventListener("change", (ev) => editor.classList.remove("stale-editor"), { once: true });
  }
}
