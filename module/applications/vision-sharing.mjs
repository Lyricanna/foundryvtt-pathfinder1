const { DocumentSheetV2, HandlebarsApplicationMixin } = foundry.applications.api;

// Add Vision Sharing sheet to ActorDirectory context options
Hooks.on("getActorDirectoryEntryContext", function addVisionSharingContextMenu(html, menu) {
  menu.push({
    name: "PF1.Application.VisionSharing.Label",
    icon: '<i class="fa-solid fa-eye"></i>',
    condition: () => game.user.isGM,
    callback: ([li]) =>
      game.actors.get(li.dataset.documentId).visionSharingSheet.render(true, {
        position: {
          // Positioning copied from Foundry's ownership dialog
          top: Math.min(li.offsetTop, window.innerHeight - 370),
          left: window.innerWidth - 720,
        },
      }),
  });
});

export class VisionSharingSheet extends HandlebarsApplicationMixin(DocumentSheetV2) {
  static DEFAULT_OPTIONS = {
    tag: "form",
    classes: ["pf1-v2", "vision-sharing"],
    window: {
      minimizable: false,
    },
    position: {
      width: 400,
      height: "auto",
    },
    form: {
      handler: this._onSubmit,
      closeOnSubmit: true,
      submitOnChange: false,
    },
    sheetConfig: false,
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/apps/vision-sharing.hbs",
    },
    footer: {
      template: "templates/generic/form-footer.hbs",
    },
  };

  get title() {
    let title = game.i18n.localize("PF1.Application.VisionSharing.Label") + `: ${this.actor.name}`;
    if (this.actor.token) title += "[" + game.i18n.localize("Token") + "]";
    return title;
  }

  async _prepareContext() {
    const context = {
      levels: {
        false: "PF1.No",
        true: "PF1.Yes",
      },
      users: game.users.players.reduce((rv, user) => {
        rv[user.id] = {
          user,
          level: null,
        };
        return rv;
      }, {}),
    };

    const config = this.actor.getFlag("pf1", "visionSharing");
    context.default = String(config?.default ?? false);
    if (config?.users) {
      Object.entries(config.users).forEach(([userId, level]) => (context.users[userId].level = String(level ?? null)));
    }

    // Footer buttons
    context.buttons = [{ type: "submit", label: "PF1.Save", icon: "fa-solid fa-save" }];

    return context;
  }

  /** @type {Actor} */
  get actor() {
    return this.document;
  }

  /**
   * @this {VisionSharingSheet}
   * @param {Event} event
   * @param {HTMLFormElement} form
   * @param {FormDataExtended} formData
   */
  static async _onSubmit(event, form, formData) {
    formData = foundry.utils.expandObject(formData.object);

    // Clear users that are using default
    for (const [userId, enabled] of Object.entries(formData.users)) {
      if (enabled) formData.users[userId] = enabled === "true";
      else formData.users[`-=${userId}`] = null;
    }

    await this.actor.setFlag("pf1", "visionSharing", formData);
  }
}
