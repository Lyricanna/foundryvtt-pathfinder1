import { ActorPF } from "./actor-pf.mjs";
import { applyChanges } from "./utils/apply-changes.mjs";

export class ActorTrapPF extends ActorPF {
  prepareBaseData() {
    this.changeFlags = {};
    /** @type {Record<string, SourceInfo>} */
    this.sourceInfo = {};
    this.system.resources ??= {};

    // Everything below this is needed for getRollData and ActorPF, but useless for the actor
    // Necessary shim for action data prep.
    this.system.abilities = {};

    this.system.attributes = {
      attack: { general: 0, shared: 0 },
      bab: { total: 0 },
      woundThresholds: {},
    };

    this.system.skills ??= {};
  }

  /**
   * @override
   */
  prepareDerivedData() {
    for (const item of this.items) {
      item._prepareDependentData(false);
      this.updateItemResources(item);
    }

    applyChanges(this);

    this._prepareCR();

    // Setup links
    this.prepareItemLinks();

    // Reset roll data cache again to include processed info
    this._rollData = null;

    // Update item resources
    for (const item of this.items) {
      item._prepareDependentData(true);
      // because the resources were already set up above, this is just updating from current roll data - so do not warn on duplicates
      this.updateItemResources(item, { warnOnDuplicate: false });
    }
  }

  /**
   * Needed to prevent unnecessary behavior in ActorPF
   *
   * @override
   */
  refreshDerivedData() {}

  _prepareCR() {
    // Reset CR
    this.system.cr.total = this.system.cr.base;

    // Reset experience value
    const newXP = pf1.utils.CR.getXP(this.system.cr.total);
    foundry.utils.setProperty(this.system, "xp.value", newXP);
  }

  /** @inheritDoc */
  _prepareTypeChanges(changes) {
    // BAB
    changes.push(
      new pf1.components.ItemChange({
        _id: "_bab", // HACK: Force ID to be special
        formula: "@attributes.bab.total",
        operator: "add",
        target: "~attackCore",
        type: "untypedPerm",
        flavor: game.i18n.localize("PF1.BAB"),
      })
    );
  }

  getRollData(options = { refresh: false, cache: true }) {
    // Return cached data, if applicable
    const skipRefresh = !options.refresh && this._rollData && options.cache;

    const result = { ...(skipRefresh ? this._rollData : foundry.utils.deepClone(this.system)) };

    // Clear certain fields if not refreshing
    if (skipRefresh) {
      for (const key of pf1.config.temporaryRollDataFields.actor) {
        delete result[key];
      }
    }

    /* ----------------------------- */
    /* Always add the following data
    /* ----------------------------- */

    // Add combat round, if in combat
    if (game.combats?.viewed) {
      result.combat = {
        round: game.combat.round || 0,
      };
    }

    // Return cached data, if applicable
    if (skipRefresh) return result;

    /* ----------------------------- */
    /* Set the following data on a refresh
    /* ----------------------------- */

    // Spoof size as Medium instead of letting it fail to Fine
    result.size = 4;

    // Spoof range as medium tall creature
    result.range = pf1.documents.actor.ActorPF.getReach();

    // Add item dictionary flags
    result.dFlags = this.itemFlags?.dictionary ?? {};

    // Call hook
    if (Hooks.events["pf1GetRollData"]?.length > 0) Hooks.callAll("pf1GetRollData", this, result);

    if (options.cache) {
      this._rollData = result;
    }

    return result;
  }

  /**
   * Get the perception modifier for the trap to detect triggers
   *
   * @returns {number} The perception modifier for the trap
   */
  getPerceptionModifier() {
    const triggerType = this.system.trigger.type;
    const visionType = this.system.trigger.vision;

    if (triggerType !== "visual") return pf1.config.trapPerceptionModifiers[triggerType] ?? 0;
    else return pf1.config.trapPerceptionModifiers.vision[visionType] ?? 0;
  }

  /**
   * Roll a perception check for the trap
   *
   * @param {object} options Options for the roll
   * @returns {Promise<void>}
   */
  async rollPerception(options = {}) {
    if (!this.isOwner) {
      return void ui.notifications.warn(game.i18n.format("PF1.Error.NoActorPermissionAlt", { name: this.name }));
    }

    const rollData = this.getRollData();
    const token = options.token ?? this.token;

    // Add metadata about the skill
    const metadata = { rank: 0 };

    const rollOptions = {
      ...options,
      parts: [`(${this.getPerceptionModifier()})[${game.i18n.localize("PF1.SkillPer")}]`],
      rollData,
      flavor: game.i18n.format("PF1.SkillCheck", { skill: game.i18n.localize("PF1.SkillPer") }),
      // chatTemplateData: { properties: props },
      compendium: { entry: pf1.config.skillCompendiumEntries["per"], type: "JournalEntry" },
      subject: { skill: "per" },
      speaker: ChatMessage.implementation.getSpeaker({ actor: this, token }),
      messageData: {
        system: {
          subject: { skill: "per" },
          config: metadata,
        },
      },
    };

    await pf1.dice.d20Roll(rollOptions);
  }

  /**
   * Get challenge rating.
   *
   * @returns {number} - CR
   */
  getCR() {
    return this.system.cr?.total ?? 0;
  }
}
