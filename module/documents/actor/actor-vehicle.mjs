import { ActorPF } from "./actor-pf.mjs";
import { getSourceInfo, getHighestChanges, applyChanges } from "./utils/apply-changes.mjs";

export class ActorVehiclePF extends ActorPF {
  prepareBaseData() {
    this._resetInherentTotals();

    // Prepare size data
    this.system.traits ??= {};
    const baseSize = this.system.traits.size || "med";
    const sizeValue = Object.keys(pf1.config.sizeChart).indexOf(baseSize);
    this.system.traits.size = {
      base: baseSize,
      value: sizeValue,
    };

    /** @type {Record<string, SourceInfo>} */
    this.sourceInfo = {};
    this.changeFlags = {};
    this.system.resources ??= {};
    const sizeMod = pf1.config.sizeMods[this.system.traits.size.base];
    const sizeModSpecial = Object.values(pf1.config.sizeSpecialMods)[this.system.traits.size.value] ?? 0;

    // Add base initiative
    this.system.attributes.init.total = this.system.attributes.init.value;

    // Handle AC
    for (const [key, value] of Object.entries(this.system.attributes.ac)) {
      value.base = 10;
      this.system.attributes.ac[key].total = value.bonus + value.base + sizeMod;
      getSourceInfo(this.sourceInfo, `system.attributes.ac.${key}.total`).positive.push(
        {
          name: game.i18n.localize("PF1.Size"),
          value: sizeMod,
          modifier: "size",
        },
        {
          name: game.i18n.localize("PF1.Modifications"),
          value: value.bonus,
        }
      );

      if (key === "stopped") {
        this.system.attributes.ac[key].total -= 7;
        getSourceInfo(this.sourceInfo, "system.attributes.ac.stopped.total").positive.push(
          {
            name: game.i18n.localize("PF1.AbilityDex"),
            value: -5,
          },
          {
            name: game.i18n.localize("PF1.Vehicles.ACStoppedMotion"),
            value: -2,
          }
        );
      }
    }

    // Handle CMD
    this.system.attributes.cmd = { base: 10 };
    this.system.attributes.cmd.total = this.system.attributes.cmd.base + sizeModSpecial;
    getSourceInfo(this.sourceInfo, "system.attributes.cmd.total").positive.push({
      name: game.i18n.localize("PF1.Size"),
      value: sizeModSpecial,
      modifier: "size",
    });
    this.system.attributes.cmd.stoppedTotal = this.system.attributes.cmd.total - 7;
    getSourceInfo(this.sourceInfo, "system.attributes.cmd.stoppedTotal").positive.push(
      {
        name: game.i18n.localize("PF1.Size"),
        value: sizeModSpecial,
        modifier: "size",
      },
      {
        name: game.i18n.localize("PF1.AbilityDex"),
        value: -5,
      },
      {
        name: game.i18n.localize("PF1.Vehicles.ACStoppedMotion"),
        value: -2,
      }
    );

    this.system.attributes.savingThrows.save.total = this.system.attributes.savingThrows.save.base;
    getSourceInfo(this.sourceInfo, `system.attributes.savingThrows.save.total`).positive.push({
      name: game.i18n.localize("PF1.Base"),
      value: this.system.attributes.savingThrows.save.base,
    });

    // Handle hardness
    this.system.attributes.hardness.base = pf1.config.vehicleMaterials[this.system.material.base]?.hardness ?? 0;
    this.system.attributes.hardness.total =
      (this.system.attributes.hardness.base + this.system.attributes.hardness.bonus) *
      (this.system.material.magicallyHardened ? 2 : 1);
    getSourceInfo(this.sourceInfo, "system.attributes.hardness.total").positive.push(
      {
        name: game.i18n.localize("PF1.Materials.Normal"),
        value: this.system.attributes.hardness.base,
      },
      {
        name: game.i18n.localize("PF1.Modifications"),
        value: this.system.attributes.hardness.bonus,
      }
    );
    if (this.system.material.magicallyHardened) {
      getSourceInfo(this.sourceInfo, "system.attributes.hardness.total").positive.push({
        name: game.i18n.localize("PF1.Vehicles.MagicallyHardened"),
        value: "x2",
      });
    }

    // Handle HP
    this.system.attributes.hp.base =
      (pf1.config.vehicleMaterials[this.system.material.base]?.hp ?? 0) * this.system.squares;
    this.system.attributes.hp.max =
      (this.system.attributes.hp.base + this.system.attributes.hp.bonus) *
      (this.system.material.magicallyHardened ? 2 : 1);
    getSourceInfo(this.sourceInfo, "system.attributes.hp.max").positive.push(
      {
        name: game.i18n.localize("PF1.Materials.Normal"),
        value: this.system.attributes.hp.base,
      },
      {
        name: game.i18n.localize("PF1.Modifications"),
        value: this.system.attributes.hp.bonus,
      }
    );
    if (this.system.squares > 0 && this.system.material.magicallyHardened) {
      getSourceInfo(this.sourceInfo, "system.attributes.hp.max").positive.push({
        name: game.i18n.localize("PF1.Vehicles.MagicallyHardened"),
        value: "x2",
      });
    }

    // Everything below this is needed for getRollData and ActorPF, but useless for the actor
    this.system.attributes.attack ??= { general: 0, shared: 0 };
    this.system.attributes.woundThresholds ??= {};
    this.system.skills ??= {};
    this.system.attributes.speed ??= {};
    this.system.attributes.cmb ??= {};

    this._prepareDriverData();
  }

  /** @inheritDoc */
  getSourceDetails(path) {
    const sources = super.getSourceDetails(path);

    const baseLabel = game.i18n.localize("PF1.Base");

    // Add base values to certain bonuses
    if (
      [
        "system.attributes.ac.normal.total",
        "system.attributes.ac.touch.total",
        "system.attributes.ac.stopped.total",
        "system.attributes.cmd.total",
        "system.attributes.cmd.stoppedTotal",
      ].includes(path)
    ) {
      sources.push({ name: baseLabel, value: 10 });
    }
  }

  /**
   * Calculate current carry capacity limits.
   *
   * Overridden to prevent unnecessary behavior in ActorPF, as vehicles don't have carry capacity.
   *
   * @override
   * @returns {{light:number,medium:number,heavy:number}} - Capacity info
   */
  getCarryCapacity() {
    return {
      light: 0,
      medium: 0,
      heavy: 0,
    };
  }

  _getInherentTotalsKeys() {
    return {
      "details.carryCapacity.bonus.total": 0,
      "details.carryCapacity.multiplier.total": 0,
    };
  }

  /**
   * @override
   * @inheritDoc
   */
  _getBaseValueFillKeys() {
    return [];
  }

  /**
   * Needed to prevent unnecessary behavior in ActorPF
   *
   * @override
   */
  prepareDerivedData() {
    for (const item of this.items) {
      item._prepareDependentData(false);
      this.updateItemResources(item);
    }

    applyChanges(this);

    this.prepareHealth();
    this._computeEncumbrance();

    this.prepareCMB();

    this._prepareOverlandSpeeds();

    // Setup links
    this.prepareItemLinks();

    // Reset roll data cache again to include processed info
    this._rollData = null;

    // Update item resources
    for (const item of this.items) {
      item._prepareDependentData(true);
      // because the resources were already set up above, this is just updating from current roll data - so do not warn on duplicates
      this.updateItemResources(item, { warnOnDuplicate: false });
    }
  }

  _prepareDriverData() {
    const driver = this.system.driver.uuid ? fromUuidSync(this.system.driver.uuid) : null;
    if (!driver) return;

    // Default to WIS mod
    let driverBase = driver.system.abilities.wis.mod;

    try {
      const skill = driver.getSkillInfo(this.system.driver.skill);
      driverBase = skill.mod;
    } catch {
      // error silently, as we default to Wisdom Modifier
    }

    this.system.driver.bonus = driverBase;
  }

  /**
   * @override
   */
  prepareCMB() {
    const size = this.system.traits.size.value,
      szCMBMod = Object.values(pf1.config.sizeSpecialMods)[size] ?? 0,
      driver = this.system.driver.bonus ?? 0;

    this.system.attributes.cmb.total = szCMBMod + driver;
  }

  prepareHealth() {
    // Offset relative health
    const hp = this.system.attributes.hp;
    if (!Number.isFinite(hp?.offset)) hp.offset = 0;
    hp.value = hp.max + hp.offset;
  }

  /**
   * Needed to prevent unnecessary behavior in ActorPF
   *
   * @override
   */
  refreshDerivedData() {}

  /** @inheritDoc */
  _prepareTypeChanges(changes) {
    changes.push(
      // BAB
      new pf1.components.ItemChange({
        _id: "_bab", // HACK: Force ID to be special
        formula: "@attributes.bab.total",
        operator: "add",
        target: "~attackCore",
        type: "untypedPerm",
        flavor: game.i18n.localize("PF1.BAB"),
      })
    );

    if (this.system.driver.uuid) {
      changes.push(
        new pf1.components.ItemChange({
          formula: "@driver.bonus",
          operator: "add",
          target: "cmb",
          type: "untyped",
          flavor: game.i18n.localize("PF1.Vehicles.DriverSkill"),
        }),
        new pf1.components.ItemChange({
          formula: "@driver.bonus",
          operator: "add",
          target: "ac",
          type: "untyped",
          flavor: game.i18n.localize("PF1.Vehicles.DriverSkill"),
        }),
        new pf1.components.ItemChange({
          formula: "@driver.bonus",
          operator: "add",
          target: "cmd",
          type: "untyped",
          flavor: game.i18n.localize("PF1.Vehicles.DriverSkill"),
        }),
        new pf1.components.ItemChange({
          formula: "floor(@driver.bonus / 2)",
          operator: "add",
          target: "vehicleSave",
          type: "untyped",
          flavor: game.i18n.localize("PF1.Vehicles.DriverSkill"),
        })
      );
    }
  }

  /** @inheritDoc */
  getRollData(options = { refresh: false, cache: true }) {
    // Return cached data, if applicable
    const skipRefresh = !options.refresh && this._rollData && options.cache;

    const result = { ...(skipRefresh ? this._rollData : foundry.utils.deepClone(this.system)) };

    // Clear certain fields if not refreshing
    if (skipRefresh) {
      for (const key of pf1.config.temporaryRollDataFields.actor) {
        delete result[key];
      }
    }

    /* ----------------------------- */
    /* Always add the following data
    /* ----------------------------- */

    // Add combat round, if in combat
    if (game.combats?.viewed) {
      result.combat = {
        round: game.combat.round || 0,
      };
    }

    // Return cached data, if applicable
    if (skipRefresh) return result;

    /* ----------------------------- */
    /* Set the following data on a refresh
      /* ----------------------------- */

    // Set size index
    const sizes = Object.values(pf1.config.sizeChart);
    result.size = Math.clamp(result.traits.size.value, 0, sizes.length - 1);

    // Add item dictionary flags
    result.dFlags = this.itemFlags?.dictionary ?? {};

    // Add range info
    result.range = { melee: 5, reach: 5 };

    // Wound Threshold isn't applicable
    result.attributes.woundThresholds = { level: 0 };

    // Traps don't have ACP
    result.attributes.acp = { attackPenalty: 0 };

    // Call hook
    if (Hooks.events["pf1GetRollData"]?.length > 0) Hooks.callAll("pf1GetRollData", this, result);

    if (options.cache) {
      this._rollData = result;
    }

    return result;
  }

  /**
   * @remarks - Vehicles don't have weightless currency
   * @override
   * @inheritDoc
   */
  getTotalCurrency({ inLowestDenomination = true } = {}) {
    const total = this.getCurrency("currency", { inLowestDenomination: true });
    return inLowestDenomination ? total : total / 100;
  }

  /**
   * Calculate overland speeds.
   * Overridden to use different data in Vehicles.
   *
   * @protected
   * @override
   */
  _prepareOverlandSpeeds() {
    this.system.details.currentSpeedOverland = pf1.utils.overlandSpeed(this.system.details.currentSpeed);
    this.system.details.maxSpeedOverland = pf1.utils.overlandSpeed(this.system.details.maxSpeed);
  }

  /**
   * Roll a specific saving throw
   *
   * @example
   * await actor.rollSavingThrow("ref", { skipDialog: true, dice: "2d20kh", bonus: "4" });
   *
   * @param {SaveId} savingThrowId Identifier for saving throw type.
   * @param {ActorRollOptions} [options={}] Roll options.
   * @returns {Promise<ChatMessage|object|void>} The chat message if one was created, or its data if not. `void` if the roll was cancelled.
   */
  async rollSavingThrow(savingThrowId, options = {}) {
    if (!this.isOwner) {
      return void ui.notifications.warn(game.i18n.format("PF1.Error.NoActorPermissionAlt", { name: this.name }));
    }

    // Add contextual notes
    const rollData = this.getRollData();
    const notes = await this.getContextNotesParsed(`savingThrow.${savingThrowId}`, { rollData });

    const parts = [];

    // Get base
    const base = this.system.attributes.savingThrows.save.base;
    if (base) parts.push(`${base}[${game.i18n.localize("PF1.Base")}]`);

    // Add changes
    let changeBonus = [];
    const changes = this.changes.filter((c) => ["vehicle", savingThrowId].includes(c.target));
    {
      // Get damage bonus
      changeBonus = getHighestChanges(
        changes.filter((c) => c.operator !== "set"),
        { ignoreTarget: true }
      ).reduce((cur, c) => {
        if (c.value)
          cur.push({
            value: c.value,
            source: c.flavor,
          });
        return cur;
      }, []);
    }
    for (const c of changeBonus) {
      parts.push(`${c.value}[${c.source}]`);
    }

    // Roll saving throw
    const props = [
      {
        header: game.i18n.localize("PF1.ConRes"),
        value: [
          { text: pf1.registry.conditions.get("bleed").name.toLowerCase() },
          { text: game.i18n.localize("PF1.AbilityDamage").toLowerCase() },
          { text: game.i18n.localize("PF1.AbilityDrain").toLowerCase() },
        ],
      },
    ];

    if (notes.length > 0) props.push({ header: game.i18n.localize("PF1.Notes"), value: notes });
    const label = pf1.config.savingThrows[savingThrowId];

    const token = options.token ?? this.token;

    const rollOptions = {
      ...options,
      parts,
      rollData,
      flavor: game.i18n.format("PF1.SavingThrowRoll", { save: label }),
      subject: { save: savingThrowId },
      chatTemplateData: { properties: props },
      speaker: ChatMessage.implementation.getSpeaker({ actor: this, token, alias: token?.name }),
    };

    if (Hooks.call("pf1PreActorRollSave", this, rollOptions, savingThrowId) === false) return;

    const result = await pf1.dice.d20Roll(rollOptions);
    Hooks.callAll("pf1ActorRollSave", this, result, savingThrowId);

    return result;
  }

  /**
   * Show defenses in chat
   *
   * @param {object} [options={}] Additional options
   * @param {string | null} [options.rollMode=null]   The roll mode to use for the roll; defaults to the user's current preference when `null`.
   * @param {TokenDocument} [options.token] Relevant token if any.
   * @returns {Promise<ChatMessage|undefined>} - Created message
   */
  async displayDefenseCard({ rollMode = null, token } = {}) {
    if (!this.isOwner) {
      return void ui.notifications.warn(game.i18n.format("PF1.Error.NoActorPermissionAlt", { name: this.name }));
    }
    const rollData = this.getRollData();

    // Add contextual AC notes
    const acNotes = await this.getContextNotesParsed("ac", { rollData });

    // Add contextual CMD notes
    const cmdNotes = await this.getContextNotesParsed("cmd", { rollData });

    // BUG: No specific saving throw notes are included
    const saveNotes = await this.getContextNotesParsed("allSavingThrows", { rollData });

    // Conditions
    const conditions = {
      bleed: pf1.registry.conditions.get("bleed").name,
      abilityDamage: game.i18n.localize("PF1.AbilityDamage"),
      abilityDrain: game.i18n.localize("PF1.AbilityDrain"),
    };

    // Get actor's token
    token ??= this.token;

    // Create message
    const actorData = this.system;
    const templateData = {
      actor: this,
      name: token?.name ?? this.name,
      tokenUuid: token?.uuid ?? null,
      type: this.system.traits.type,
      ac: {
        normal: actorData.attributes.ac.normal.total,
        touch: actorData.attributes.ac.touch.total,
        stopped: actorData.attributes.ac.stopped.total,
        notes: acNotes,
      },
      cmd: {
        normal: actorData.attributes.cmd.total,
        stopped: actorData.attributes.cmd.stoppedTotal,
        notes: cmdNotes,
      },
      misc: {
        hardness: actorData.traits.hardness,
        conditions,
      },
      saves: {
        save: rollData.attributes?.savingThrows?.save?.total,
        notes: saveNotes,
      },
    };

    rollMode ||= game.settings.get("core", "rollMode");

    const chatData = {
      content: await renderTemplate("systems/pf1/templates/chat/defenses-vehicle.hbs", templateData),
      speaker: ChatMessage.implementation.getSpeaker({ actor: this, token, alias: token?.name }),
      rollMode,
      system: {
        subject: { info: "defenses" },
      },
      flags: {
        core: {
          canPopout: true,
        },
      },
    };

    // Apply roll mode
    ChatMessage.implementation.applyRollMode(chatData, rollMode);

    return ChatMessage.implementation.create(chatData);
  }
}
