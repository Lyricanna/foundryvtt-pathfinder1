/**
 * Base model for Active Effects
 *
 * Active Effect type: `base` (the default when unspecified)
 */
export class AEBaseModel extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    const fields = foundry.data.fields;

    return {
      // End timing
      end: new fields.StringField({
        required: false,
        blank: false,
        choices: () => pf1.config.durationEndEvents,
        label: "PF1.DurationEndTiming",
      }),
      // Initiative this AE was started on
      initiative: new fields.NumberField({
        required: false,
        initial: undefined, // HACK: Without unlinked actors start erroring about nulls
        nullable: false,
        label: "PF1.Initiative",
      }),
      // Arbitrary level
      level: new fields.NumberField({
        required: false,
        initial: undefined, // HACK: Without unlinked actors start erroring about nulls
        integer: true,
        nullable: false,
        label: "PF1.Level",
      }),
    };
  }

  /**
   * Retrieve parent item
   *
   * @type {Item|null}
   */
  get item() {
    const doc = this.parent?.parent;
    if (doc instanceof Item) return doc;
    return null;
  }
}
