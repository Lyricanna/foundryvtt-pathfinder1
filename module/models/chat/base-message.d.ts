export {};

declare module "./base-message.mjs" {
  interface BaseMessageModel {
    /**
     * Subject info object
     */
    subject?: object;
    /**
     * Reference to what triggered this message if anything.
     */
    reference?: string;
    /**
     * Combat ID if this is message relates to any specific combat encounter.
     */
    combat?: string;
    /**
     * Arbitrary config object
     */
    config?: object;
  }
}
