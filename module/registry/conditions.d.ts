export {};

declare module "./conditions.mjs" {
  interface Condition extends RegistryEntry {
    texture: string;

    track: keyof typeof Conditions.TRACKS | null;
    mechanics: {
      changes: object[];
      flags: Set<string>;
      contexts: Set<string>;
    };
    journal: string;
    showInDefense: boolean;
    showInAction: boolean;
    showInBuffsTab: boolean;
    hud: {
      show: boolean;
      /**
       * Show for actor types
       */
      include: Set<string>;
      /**
       * Exclude from actors.
       *
       * Has no effect if include set is defined
       */
      exclude: Set<string>;
    };
  }
}
