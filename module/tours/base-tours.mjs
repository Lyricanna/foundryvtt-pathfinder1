export class PF1Tour extends Tour {
  /**
   * An array of {@link Application}s that are currently open.
   * This needs manual tracking from the classes that inherit from `PF1Tour` and
   * render the applications themselves.
   *
   * @type {Array<Application>}
   */
  apps = [];

  /**
   * @returns {TourStep} The previous step.
   */
  get previousStep() {
    return this.hasPrevious ? this.steps[this.stepIndex - 1] : undefined;
  }

  /**
   * Gets the current sheet based on if it's displayed or not.
   * WARNING: This won't work if the actor sheet is not displayed or there are multiple actor sheets open at once.
   *
   * @returns {pf1.applications.actor.ActorSheetPF | undefined} The current actor sheet if found.
   */
  get sheetInDisplay() {
    // We need to Actor ID to find the sheet. The actor ID is in the ID of the sheet element but needs some formatting
    return Object.values(ui.windows).find((app) => app instanceof ActorSheet);
  }

  /**
   * Based on the `steps` variable, it returns an object with the normalized step IDs as keys and the step IDs as values.
   * The formatting will take `step-1` and return `STEP_1`.
   *
   * @returns {Record<string, string>} An object with the normalized step IDs as keys and the step IDs as values.
   */
  get StepsEnum() {
    return this.steps.reduce((steps, step) => {
      const stepKey = step.id.replaceAll(/-/g, "_").toUpperCase();
      steps[stepKey] = step.id;
      return steps;
    }, {});
  }

  /**
   * Given the ID of a step, returns the step index associated with it.
   *
   * @param {string} id The ID of the step.
   * @returns {number} The step index with the given ID (-1 if not found).
   */
  getStepIndexById(id) {
    return this.steps.findIndex((step) => step.id === id);
  }

  /**
   * In many steps we might need an specific {@link pf1.applications.compendiumBrowser.CompendiumBrowser} to be opened
   * to continue. This function opens the given type of compendium and stores it for later closing or referencing.
   *
   * @param {string} compendium - The compendium to open.
   * @param {object} [options={}] - Additional options to pass to the render function.
   * @see {@link pf1.applications.compendiumBrowser.CompendiumBrowser.initializeBrowsers}
   * @see {@link Application._render}
   *
   * @returns {Promise<pf1.applications.compendiumBrowser.CompendiumBrowser>} The opened compendium.
   */
  async openCompendiumBrowser(compendium, options = {}) {
    // Open compendium
    const comp = pf1.applications.compendiums[compendium];
    if (!comp) {
      throw new Error(`Compendium "${compendium}" not found`);
    }
    // Set current compendium
    await comp._render(true, { focus: true, ...options });
    // Add CompendiumBrowser to apps
    this.apps.push(comp);

    return comp;
  }

  /**
   * Same as {@link openCompendiumBrowser} but for {@link CompendiumCollection}.
   *
   * @param {string} compendium - The compendium to open.
   * @param {object} [options={}] - Additional options to pass to the render function.
   * @see {@link openCompendiumBrowser}
   * @see {@link Application._render}
   * @see {@link Compendium}
   *
   * @returns {Promise<CompendiumCollection>} The opened compendium.
   */
  async openCompendium(compendium, options = {}) {
    /** @type {CompendiumCollection} */
    const comp = game.packs.get(compendium, { strict: true });

    // Open compendium
    const compApp = comp.apps.find((app) => app instanceof comp.applicationClass);
    // Render and add to apps
    await compApp._render(true, { focus: true, ...options });
    // Add Compendium to apps
    this.apps.push(compApp);

    return comp;
  }

  /**
   * Closes the currently opened {@link pf1.applications.compendiumBrowser.CompendiumBrowser} or {@link Compendium} either
   * by referencing the variable or checking if the active window is the compendium.
   *
   * @see {@link openCompendiumBrowser}
   * @see {@link Application.close}
   *
   * @returns {Promise<void>}
   */
  async closeCompendium() {
    const compApps = this.apps.filter(
      (app) => app instanceof pf1.applications.compendiumBrowser.CompendiumBrowser || app instanceof Compendium
    );

    for (const app of compApps) {
      this._debug(`Closing \`${app.constructor.name}\` ${app.id}`);
      await app.close();
    }
  }

  /**
   * This log functions outputs the given text to the console with Tour ID in the log message.
   *
   * @param {string} text The text to log.
   * @param {"log"|"debug"|"warn"} loglevel The log level to use.
   * @param {...any} args Extra arguments to pass to {@link console.info}.
   */
  _log(text, loglevel = "log", ...args) {
    console[loglevel](`[Tour Step "%s.%s.%s"]: ${text}`, ...[this.namespace, this.id, this.currentStep?.id, ...args]);
  }

  /**
   * @param {string} text The text to log.
   * @param  {...any} args Extra arguments to pass to {@link console.info}.
   * @see {@link _log}
   */
  _debug(text, ...args) {
    this._log(text, "debug", ...args);
  }

  /**
   * @param {string} text The text to log.
   * @param  {...any} args Extra arguments to pass to {@link console.info}.
   * @see {@link _log}
   */
  _warn(text, ...args) {
    this._log(text, "warn", ...args);
  }

  /**
   * Expands a compendium folder and parent folders if found in the Compendium tab.
   * Ultimately returns the direct compendium folder.
   *
   * @param {string} pack - The name of the compendium from which to get the folder.
   *
   * @returns {Folder | null} The actors folder.
   */
  expandCompendiumFolder(pack) {
    // We'll retrieve the element based on the ID and for that we'll need the folder object from compendium
    const compendium = game.packs.get(pack, { strict: true });
    /** @type {Folder} */
    const folder = compendium.folder;

    if (!folder) {
      this._debug(
        "Couldn't find the folder ID for the Basic Monsters compendium. It could be that the compendium isn't in a folder."
      );
      return null;
    }

    /**
     * Given a folder it removes the `collapse` tag expanding it and searches for a parent folder in order to to the same.
     *
     * @param {Folder} folder - The folder to expand.
     */
    const clickFolder = (folder) => {
      if (!folder.expanded) {
        /** @type {HTMLLIElement | null} */
        const folderEl = document.querySelector(`li.directory-item.folder[data-uuid="Folder.${folder.id}"]`);
        // The one that triggers the click is the header
        folderEl?.querySelector("header")?.click();
      }

      // If this folder is child of another folder we want to expand it as well
      if (folder.folder) {
        clickFolder(folder.folder);
      }
    };

    clickFolder(folder);

    return folder;
  }

  /** @override */
  async _preStep() {
    if (this.currentStep.sheetTab) {
      this._debug(
        `(${this._preStep.name}) Activating ${this.currentStep.sheetTab.tabName} (${this.currentStep.sheetTab.group}) tab`
      );
      this.sheetInDisplay?.activateTab(this.currentStep.sheetTab.tabName, this.currentStep.sheetTab.group);
    }

    await super._preStep();
  }
}
