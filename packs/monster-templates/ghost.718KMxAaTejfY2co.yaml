_id: 718KMxAaTejfY2co
_key: '!items!718KMxAaTejfY2co'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/violet_07.jpg
name: Ghost
system:
  acquired: true
  changes:
    - _id: 0wwuebw6
      formula: '@abilities.cha.mod'
      target: ac
      type: deflection
    - _id: njfnkfir
      formula: '-max(0, @ac.natural.total)'
      target: nac
      type: untyped
    - _id: 7919odge
      formula: '30'
      operator: set
      target: flySpeed
      type: base
    - _id: im5ym2hw
      formula: '4'
      target: cha
      type: untyped
    - _id: hQmBGFTl
      formula: '0'
      operator: set
      target: landSpeed
      type: untypedPerm
    - _id: pt7io8qj
      formula: '8'
      target: skill.per
      type: racial
    - _id: Tbu6aTiC
      formula: '8'
      target: skill.ste
      type: racial
  classSkills:
    clm: true
    dis: true
    fly: true
    int: true
    kar: true
    kre: true
    per: true
    sen: true
    spl: true
    ste: true
  crOffset: '2'
  creatureSubtypes:
    - incorporeal
  creatureTypes:
    - undead
  description:
    instructions: >-
      <p>Change all <strong>racial HD</strong> to <strong>D8s</strong>.</p><p>In
      <strong>Settings tab</strong> change <strong>Ability Score Links</strong>
      on <strong>Hit Points</strong> to <strong>Charisma</strong>.</p><p>Change
      <strong>Fly Maneuverability</strong> to
      <strong>Perfect</strong>.</p><p>Add the extra <strong>Special
      Attacks</strong> depending on the creature's HD.</p><p>Change
      <strong>Strength</strong> and <strong>Constitution</strong> in the
      <strong>Attributes tab</strong> and set them to <strong>-</strong>.</p>
    value: >-
      <p><strong>Acquired/Inherited Template</strong> Acquired<br
      /><strong>Simple Template</strong> No<br /><strong>Usable with
      Summons</strong> Yes</p><p>"Ghost" is an acquired template that can be
      added to any living creature that has a Charisma score of at least 6. A
      ghost retains all the base creature's statistics and special abilities
      except as noted here.</p><p><strong>Challenge Rating:</strong> Same as the
      base creature +2.</p><p><strong>Type:</strong> The creature's type changes
      to undead. Do not recalculate the creature's base attack bonus, saves, or
      skill points. It gains the incorporeal subtype.</p><p><strong>Armor
      Class:</strong> A ghost gains a deflection bonus equal to its Charisma
      modifier. It loses the base creature's natural armor bonus, as well as all
      armor and shield bonuses not from force effects or ghost touch
      items.</p><p><strong>Hit Dice:</strong> Change all of the creature's
      racial Hit Dice to d8s. All Hit Dice derived from class levels remain
      unchanged. Ghosts use their Charisma modifiers to determine bonus hit
      points (instead of Constitution).</p><p><strong>Defensive
      Abilities:</strong> A ghost retains all of the defensive abilities of the
      base creature save those that rely on a corporeal form to function. Ghosts
      gain channel resistance +4, darkvision 60 ft., the incorporeal ability,
      and all of the immunities granted by its undead traits. Ghosts also gain
      the @UUID[Compendium.pf1.template-abilities.Item.07oHIjzqmAhroY6y]
      ability.</p><p><strong>Speed:</strong> Ghosts lose their previous speeds
      and gain a fly speed of 30 feet (perfect), unless the base creature has a
      higher fly speed.</p><p><strong>Melee and Ranged Attacks:</strong> A ghost
      loses all of the base creature's attacks. If it could wield weapons in
      life, it can wield ghost touch weapons as a ghost.</p><p><strong>Special
      Attacks:</strong> A ghost retains all the special attacks of the base
      creature, but any relying on physical contact do not function. In
      addition, a ghost gains one ghost special attack from the list below for
      every 3 points of CR (minimum 1—the first ability chosen must always be
      <em>corrupting touch</em>). The save DC against a ghost's special attack
      is equal to 10 + 1/2 ghost's HD + ghost's Charisma modifier unless
      otherwise noted. Additional ghost abilities beyond these can be designed
      at the GM's
      discretion.</p><ul><li><p>@UUID[Compendium.pf1.template-abilities.Item.pOEFIy4STD3Rcokx]</p></li><li><p>@UUID[Compendium.pf1.template-abilities.Item.FsbKN8sAOmcUvF1f]</p></li><li><p>@UUID[Compendium.pf1.template-abilities.Item.6Ixo1uICjdRLDFaY]</p></li><li><p>@UUID[Compendium.pf1.template-abilities.Item.qL3lKcBF2jJjcf55]</p></li><li><p>@UUID[Compendium.pf1.template-abilities.Item.ra4nVjZDEAZgu5JW]</p></li><li><p>@UUID[Compendium.pf1.template-abilities.Item.pNbKBq0ewMnYlXhu]</p></li></ul><p><strong>Abilities:</strong>
      Cha +4; as an incorporeal undead creature, a ghost has no Strength or
      Constitution score.</p><p><strong>Skills:</strong> Ghosts have a +8 racial
      bonus on Perception and Stealth skill checks. A ghost always treats Climb,
      Disguise, Fly, Intimidate, Knowledge (arcana), Knowledge (religion),
      Perception, Sense Motive, Spellcraft, and Stealth as class skills.
      Otherwise, skills are the same as the base creature.</p>
  links:
    supplements:
      - uuid: Compendium.pf1.monster-abilities.Item.1e5wGWxYWt0maxmB
      - uuid: Compendium.pf1.monster-abilities.Item.abPQPu23CmkfWudq
      - uuid: Compendium.pf1.monster-abilities.Item.kcCRPAHsrWouTbij
      - uuid: Compendium.pf1.template-abilities.Item.07oHIjzqmAhroY6y
      - uuid: Compendium.pf1.monster-abilities.Item.ZB6Insc46LaVWNvR
      - uuid: Compendium.pf1.template-abilities.Item.FsbKN8sAOmcUvF1f
  sources:
    - id: PZO1112
      pages: '144'
  subType: template
  summons: true
type: feat
