_id: YE9x2cUVGiNibXuu
_key: '!items!YE9x2cUVGiNibXuu'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/violet_07.jpg
name: Hell Engine
system:
  crOffset: '1'
  description:
    value: >-
      <p><strong>Acquired/Inherited Template</strong> Acquired<br
      /><strong>Simple Template</strong> No<br /><strong>Usable with
      Summons</strong> No</p><p>A hell engine is a construct fueled by an
      infernal contract, bypassing the complex magic and craftsmanship needed to
      animate a golem or similar engine of destruction. Artificers who lack the
      skill or magical aptitude to craft constructs on their own may be tempted
      to use the power of Hell to make up for their shortcomings, surrendering
      their souls for the chance to see their masterpieces brought to horrifying
      life.</p><p>Although a hell engine lacks the creativity and cunning of a
      true devil, contracting an infernal construct grants certain advantages
      over a bound outsider. A hell engine’s mindless neutrality renders it
      resistant to anarchic or holy attacks that would cripple a devil, making
      it a valuable tool against celestial and chaotic forces. Combined with the
      construct’s ability to banish hostile outsiders and replace them with
      additional devils, these war machines earn a distinguished place as shock
      troops in infernal armies and guardians of Hell’s most secure
      vaults.</p><p>"Hell engine" is an acquired template that can be added to
      any nonchaotic, nongood construct with no Intelligence score. A hell
      engine uses all the base creature’s statistics and special abilities
      except as noted here.</p><p><strong>Challenge Rating:</strong> Base
      creature’s CR + 1.</p><p><strong>Alignment:</strong> Always neutral.
      However, a hell engine radiates a moderate aura of law and evil as if it
      were a lawful evil outsider.</p><p><strong>Senses:</strong> A hell engine
      gains the see in darkness ability. In addition, the hell engine can see
      through the hellfire cloud created by its breath weapon without penalty,
      ignoring any cover or concealment bonuses it
      provides.</p><p><strong>Defensive Abilities:</strong> A hell engine gains
      fire resistance 30. If the base creature has immunity to magic, it gains
      the following
      change:</p><ul><li>@UUID[Compendium.pf1.spells.Item.sg3eq6xpsum65fgm]{Dispel
      evil} or @UUID[Compendium.pf1.spells.Item.fhuoxhjgf9otqvzh]{dispel law}
      drives the hell engine back 30 feet and deals 2d12 points of damage to it
      (no save).</li></ul><p><strong>Weaknesses:</strong> A hell engine gains
      the following weakness.</p><p><em>Contract Powered (Ex):</em> A hell
      engine draws power from the infernal contract that animates it. A hell
      engine cannot attack the devil that drafted its contract, the mortal who
      signed it, or any creature holding an original copy of the contract.
      Spells cast by the devil that wrote the contract or its mortal signatory
      automatically bypass any spell resistance or immunity to magic the hell
      engine has. If both copies of the contract are destroyed, the hell engine
      ceases to function until a new one is created.</p><p><strong>Special
      Attacks:</strong> A hell engine gains the following.</p><p><em>Breath
      Weapon (Su)</em>: As a standard action once every 1d4+1 rounds, a hell
      engine can exhale a churning cloud of hellfire in an adjacent space equal
      to its own size. This hellfire persists for 1 round; each creature within
      the area when the hell engine creates it (as well as any creature that
      passes through the cloud until the start of the hell engine’s next turn)
      takes 1d6 points of fire damage and 1d6 points of unholy damage per 2 Hit
      Dice of the base creature. A creature can attempt a Reflex save (DC 10 +
      half the base creature’s HD) for half damage. The hellfire cloud also
      provides concealment as if from a
      @UUID[Compendium.pf1.spells.Item.g33euis7yi9pwddy]{fog cloud}
      spell.</p><p>The hell engine can use this breath weapon in addition to any
      breath weapon the base creature has, and it can use up to two breath
      weapons simultaneously as a full-round action. Simultaneous breath weapons
      both fill the area of either the hellfire breath weapon or base creature’s
      breath weapon (hell engine’s choice). If the base creature’s breath weapon
      deals fire damage, a simultaneous breath weapon converts half of that
      damage into unholy damage.</p><p><em>Banishing Strike (Su):</em> Three
      times per day as an immediate action, the hell engine can force an
      extraplanar or summoned creature it hits to attempt a Will save (DC = 10 +
      half the base creature’s HD); on a failure, the target is forced back to
      its original plane as if by a
      @UUID[Compendium.pf1.spells.Item.k3zn13pbr5tr9zac]{dismissal}
      spell.</p><p><em>Redirect Summons (Sp): </em>Within 1 minute of
      successfully using its banishing strike ability, a hell engine can
      redirect its planar energy to summon a devil as an immediate action with a
      100% chance of success. The summoned devil remains for 1 hour. A hell
      engine can have only one summoned devil at a time. The hell engine’s Hit
      Dice determines the most powerful kind of devil it can summon and the
      effective spell level of this ability, according to the following
      table.<br /><br
      /></p><table><tbody><tr><td><p><strong>HD</strong></p></td><td><p><strong>Devil</strong></p></td><td><p><strong>Spell
      Level</strong></p></td></tr><tr><td><p>5</p></td><td><p><a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Lemure">Lemure</a></p></td><td><p>2nd</p></td></tr><tr><td><p>10</p></td><td><p><a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Bearded%20Devil%20(Barbazu)">Bearded
      devil</a></p></td><td><p>5th</p></td></tr><tr><td><p>15</p></td><td><p><a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Erinyes">Erinyes</a></p></td><td><p>6th</p></td></tr><tr><td><p>20</p></td><td><p><a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Bone%20Devil%20(Osyluth)">Bone
      devil</a></p></td><td><p>7th</p></td></tr><tr><td><p>25</p></td><td><p><a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Barbed%20Devil%20(Hamatula)">Barbed
      devil</a></p></td><td><p>8th</p></td></tr><tr><td><p>30+</p></td><td><p><a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Ice%20Devil%20(Gelugon)">Ice
      devil</a></p></td><td><p>9th</p></td></tr></tbody></table>
  subType: template
type: feat
