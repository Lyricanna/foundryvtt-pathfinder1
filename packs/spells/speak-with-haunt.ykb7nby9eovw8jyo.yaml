_id: ykb7nby9eovw8jyo
_key: '!items!ykb7nby9eovw8jyo'
_stats:
  coreVersion: '12.331'
folder: 9hBqwSiT8GIHl0Ht
img: icons/magic/death/undead-ghost-scream-teal.webp
name: Speak with Haunt
system:
  actions:
    - _id: x02va8c175l36hcg
      actionType: spellsave
      activation:
        cost: 10
        type: minute
        unchained:
          cost: 10
          type: minute
      duration:
        units: minute
        value: '@cl'
      name: Use
      range:
        units: ft
        value: '10'
      save:
        description: Will negates (see text)
        type: will
      target:
        value: one haunt
  components:
    divineFocus: 1
    somatic: true
    verbal: true
  description:
    value: >-
      <p>You stir a haunt (<i>Pathfinder RPG GameMastery Guide</i> 242) to a
      limited sense of awareness and consciousness, allowing it to answer
      questions. The spell's range must reach any square within 10 feet of the
      haunt's area. You must be aware of the haunt prior to casting the spell,
      and casting the spell does not trigger the haunt. You can ask one question
      per 2 caster levels. The haunt's knowledge is limited to what its original
      creature knew during life, including the languages it spoke. A haunt often
      remembers the circumstance that led to its existence (though this
      recollection might be from the original victim's perspective and therefore
      not objective), what triggers it, and how it can be laid to rest
      (destroyed). Answers are brief, cryptic, or repetitive, especially if the
      haunt is angry and vindictive.</p><p>If the haunt's alignment is more than
      one step away from yours, the haunt can attempt a Will save to resist the
      spell.</p><p>A haunt's Will save modifier is equal to 3 + the haunt's
      CR.</p><p>If the save is successful, the haunt can refuse to answer your
      questions or attempt to deceive you (using Bluff). A haunt's Bluff
      modifier equals its CR (minimum +0) or might be determined by the GM based
      upon the original victim.</p><p>The haunt can speak only about what it
      knew in life and the circumstances by which it became a haunt. It cannot
      answer any questions that pertain to events that occurred after it was
      created. A neutral or good haunt might cooperate with similarly aligned
      creatures in order to end its suffering.</p><p>If a haunt has been subject
      to this spell within the past week, a new casting of this spell on it
      fails. You can cast this on a haunt that has been deceased for any amount
      of time.</p><p>Unlike a corpse affected by <i>speak with dead</i>, a haunt
      wants to express itself, if only to share its pain or to cause
      mischief.</p>
  descriptors:
    - languageDependent
  learnedAt:
    class:
      cleric: 4
      medium: 2
      oracle: 4
      psychic: 4
      shaman: 3
      spiritualist: 4
      warpriest: 4
      witch: 4
  level: 4
  school: nec
  sources:
    - id: PZO1129
      pages: '193'
  sr: false
type: spell
