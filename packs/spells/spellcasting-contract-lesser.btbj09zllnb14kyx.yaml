_id: btbj09zllnb14kyx
_key: '!items!btbj09zllnb14kyx'
_stats:
  coreVersion: '12.331'
folder: NE0tEwSvcr71GnPC
img: systems/pf1/icons/misc/magic-swirl.png
name: Spellcasting Contract, Lesser
system:
  actions:
    - _id: 1c45nw2pxvm5x99k
      actionType: spellsave
      activation:
        cost: 10
        type: minute
        unchained:
          cost: 10
          type: minute
      duration:
        units: spec
        value: permanent until contractually terminated
      name: Use
      range:
        units: touch
      save:
        description: Will negates
        harmless: true
        type: will
      target:
        value: willing creature touched
  components:
    focus: true
    somatic: true
    verbal: true
  description:
    value: >-
      <p>This spell functions exactly like <i>imbue with spell ability</i>,
      except that you can imbue the target with any spell you have prepared
      (instead of just abjuration, divination, or conjuration [healing] spells)
      and the target may have more than one use of the imbued spells, depending
      upon the arrangements made when it is cast.</p><p>Casting this spell
      requires a contract between you and the target, explaining what spells are
      to be imbued and the circumstances that cause the contract to expire. The
      contract may be as simple as allowing the target one casting of each of
      the imbued spells (as per <i>imbue with spell ability</i>), or may
      continue for multiple days or even indefinitely, with the target regaining
      use of the imbued spells when you next prepare your own spells. You may
      include any proviso you see fit, such as requiring the target to pray to
      Asmodeus each morning, or restricting the target to only casting the
      imbued spells on himself. If the target does not agree to all the
      conditions in the contract, this spell fails when cast. The contract (and
      this spell) automatically expires if you or the target dies. While the
      contract remains in effect, you gain a profane bonus to your Armor Class,
      saving throws, and checks equal to the highestlevel spell you have
      imbued.</p><p>Once you cast this spell, you cannot prepare a new 5th-level
      spell to replace it until the contract expires. If the number of 5th-level
      spells you can cast decreases, and that number drops below your current
      number of active <i>lesser spellcasting contract</i> spells, the more
      recently cast imbued spells are dispelled.</p><p>Unlike <i>imbue with
      spell ability</i>, how the target uses the spell has no reflection on your
      alignment or relationship with Asmodeus; the Prince of Darkness accepts
      that allowing another access to his magic for good may benefit his plans
      in the long run. Note that unlike <i>imbue with spell ability</i>, you
      cannot dismiss this spell; you must abide by the contract's termination
      clause (though the contract may include a proviso for at-will
      nullification by either or both parties). This spell cannot be combined
      with <i>imbue with spell ability</i> or similar spells to give a target
      more spells than the limit.</p><p><i>Example:</i> You cast this spell on
      your 5 HD fighter cohort after negotiating an appropriate contract,
      imbuing him with the ability to cast cure moderate wounds, magic weapon,
      and <i>Shield of Faith</i> once per day for 1 month. If he casts any of
      these spells, he recovers them when you prepare your spells. Until the
      contract ends, your 5th-level spell slot used to cast this spell remains
      expended and cannot be filled with a new spell. Because you imbued your
      cohort with a 2nd-level spell, you gain a +2 profane bonus to attacks,
      saves, and checks while the contract remains in effect.</p>
  learnedAt:
    class:
      cleric: 5
      oracle: 5
      warpriest: 5
  level: 5
  materials:
    focus: a written contract
  school: evo
  sources:
    - id: PZO9029
      pages: '67'
type: spell
